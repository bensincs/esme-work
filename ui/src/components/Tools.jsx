import React, { Component } from 'react'
import {Table, Button, Container, Modal} from 'react-bootstrap'
import AddTool from './AddTool.jsx'

class Tools extends Component {
  constructor(props){
    super(props);
    this.state = {allTools: [], toolToEdit : {}, showEdit : false};
    this.getTools();

    this.getTools = this.getTools.bind(this)
  }

  getTools(){
    var self = this;
    fetch('http://localhost:8080/tool')
    .then(function(response) {
      return response.json();
    })
    .then(function(response) {
      self.setState({allTools: response});
    });
  }

  borrow(id){
    var self = this;
    var myHeaders = {
      'Session-Id' : "2" //change to be cookie. The cookie will be created when a user logs in. Done in Login page
    }
    fetch('http://localhost:8080/tool/'+id+'/borrow', {headers : myHeaders})
    .then(function(response) {
      self.getTools();
    });
  }

  showEdit(tool){
    this.setState({toolToEdit: tool, showEdit: true})
  }

  render() {

    var rows = [];

    for (var i=0; i<this.state.allTools.length; i++) {
        var tool = this.state.allTools[i]

        var condition;
        if(tool.used){
          condition = "Used"
        }else{
          condition = "New"
        }

        var borrowed;
        if(tool.borrowed){
          borrowed = tool.borrowedById
        }else{
          borrowed = <Button onClick={() => this.borrow(tool.toolId)} >Borrow</Button>
        }

        rows.push(
        <tr>
          <td>{tool.name}</td>
          <td>{tool.description}</td>
          <td>{condition}</td>
          <td>{borrowed}<Button onClick={() => this.showEdit(tool)} >Edit</Button></td>
        </tr>
      );
    }
    return (
      <div>
      <AddTool getTools={this.getTools}/>
      <Table responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Condition</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        {rows}
        </tbody>
      </Table>
      </div>





      //Create a bootstrap form. That allows a tool to be added. It will have inputs for each variable and wiil have a submit button. The sibmit button will send a fecth reqyest to API. to create a tool.
    );
  }


}

export default Tools;
