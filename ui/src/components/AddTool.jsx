import React, { Component } from 'react'
import {Table, Panel, Button, Container, FormGroup, FormControl} from 'react-bootstrap'
import AddTool from './AddTool'

class Tools extends Component {
  constructor(props){
    super(props);
    this.state = {
      name : '',
      description: '',
    }

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleNameChange(e){
    this.setState({name : e.target.value})
  }

  handleDescriptionChange(e){
    this.setState({description : e.target.value})
  }

  handleSubmit(){

    var self = this;
    fetch("http://localhost:8080/tool",
    {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Session-Id': '2'
        },
        method: "POST",
        body: JSON.stringify(this.state)
    }).then(function(res){ return res.json() })
    .then(function(res){
      self.props.getTools();
    });
  }


  render() {
    return (

      <Panel id="collapsible-panel-example-2" defaultExpanded>
                <Panel.Heading>
                  <Panel.Title toggle>
                  Add Tool
                  </Panel.Title>
                </Panel.Heading>
                <Panel.Collapse>
                  <Panel.Body>
                  <form>
                          <FormGroup
                            controlId="formBasicText"
                          >
                            <FormControl
                              type="text"
                              value={this.state.name}
                              placeholder="Tool Name"
                              onChange={this.handleNameChange}
                            />
                            <br/>
                            <FormControl
                              type="text"
                              value={this.state.description}
                              placeholder="Description"
                              onChange={this.handleDescriptionChange}
                            />

                          </FormGroup>
                          <Button onClick={this.handleSubmit}>Submit</Button>
                        </form>
                  </Panel.Body>
                </Panel.Collapse>
              </Panel>




    );
  }


}

export default Tools;
