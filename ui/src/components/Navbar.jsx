import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

class MyNavbar extends Component {
  render() {
    return (
      <Navbar>
<Navbar.Header>
  <Navbar.Brand>
    <a href="#home">React-Bootstrap</a>
  </Navbar.Brand>
</Navbar.Header>
<Nav>
  <NavItem href="/tools">
    Tools
  </NavItem>
  <NavItem  href="/my-tools">
    My Tools
  </NavItem>
  <NavItem  href="/account">
    Account
  </NavItem>
</Nav>
</Navbar>
    );
  }
}

export default MyNavbar;
