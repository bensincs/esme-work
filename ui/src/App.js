import React, { Component } from 'react';
import Navbar from './components/Navbar.jsx'
import Tools from './components/Tools.jsx'
import {Grid, Row, Col} from 'react-bootstrap'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Navbar />

        <Grid>
          <Row className="show-grid">
            <Col md={1}>

            </Col>
            <Col md={10}>

                    <Tools />
            </Col>
            <Col md={1}>

            </Col>

          </Row>
        </Grid>

      </div>
    );
  }
}

export default App;
