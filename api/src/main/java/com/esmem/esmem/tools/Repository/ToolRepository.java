package com.esmem.esmem.tools.Repository;

import com.esmem.esmem.tools.Model.Tool;
import org.springframework.data.repository.CrudRepository;

public interface ToolRepository extends CrudRepository<Tool, Long> {
}
