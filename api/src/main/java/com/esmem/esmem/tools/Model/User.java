package com.esmem.esmem.tools.Model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {

    @Id //marks as pk
    private String username;
    private String password;
    private String mobile;
    private String email;
    private String postcode;


    public User(String username, String password, String mobile, String email, String postcode) {
        this.username = username;
        this.password = password;
        this.mobile = mobile;
        this.email = email;
        this.postcode = postcode;
    }

    public User() { //blank constructor used by jpa   //jpa is a java persistence lib that allows you to CRUD entities
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
}
