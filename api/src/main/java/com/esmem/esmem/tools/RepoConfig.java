package com.esmem.esmem.tools;

import com.esmem.esmem.tools.Model.Session;
import com.esmem.esmem.tools.Model.Tool;
import com.esmem.esmem.tools.Model.User;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class RepoConfig extends RepositoryRestConfigurerAdapter {

    //Spring config to expose ids of entities

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(Session.class, Tool.class, User.class);
    }
}