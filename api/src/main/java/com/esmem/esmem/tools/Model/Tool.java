package com.esmem.esmem.tools.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Tool {

    @GeneratedValue
    @Id
    private Long toolId;
    private String name;
    private String description;
    private String category;
    private boolean isUsed;
    private String borrowedById;
    private String owner;

    public Tool(Long toolId, String name, String description, String category, boolean isUsed, String borrowedById) {
        this.toolId = toolId;
        this.name = name;
        this.description = description;
        this.category = category;
        this.isUsed = isUsed;
    }

    public Tool() {
    }

    public Long getToolId() {
        return toolId;
    }

    public void setToolId(Long toolId) {
        this.toolId = toolId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isUsed() {
        return isUsed;
    }

    public void setUsed(boolean used) {
        isUsed = used;
    }

    public String getBorrowedById() {
        return borrowedById;
    }

    public void setBorrowedById(String borrowedById) {
        this.borrowedById = borrowedById;
    }

    public boolean isBorrowed(){
        return this.getBorrowedById() != null;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
