package com.esmem.esmem.tools.Controller;

import com.esmem.esmem.tools.Model.User;
import com.esmem.esmem.tools.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class UserController
{
    @Autowired
    UserRepository repository; //gives access to the repository

    @RequestMapping(method = RequestMethod.POST, path = "/user")
    public User createUser(@RequestBody User user){
        User newUser = user;
        //validate here
        repository.save(newUser); //saves new user to the repos
        return newUser;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/user/{id}")
    public Optional<User> getUser(@PathVariable("id") String id){
        return repository.findById(id);
    }

    @RequestMapping(method = RequestMethod.GET, path="/user")
    public Iterable<User> getUsers(){
        return repository.findAll();
    }
}
