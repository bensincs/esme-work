package com.esmem.esmem.tools.Repository;

import com.esmem.esmem.tools.Model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, String> {
}
