package com.esmem.esmem.tools.Repository;

import com.esmem.esmem.tools.Model.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session, Long> {

}
