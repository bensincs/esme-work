package com.esmem.esmem.tools.Controller;

import com.esmem.esmem.tools.Model.Session;
import com.esmem.esmem.tools.Model.User;
import com.esmem.esmem.tools.Repository.SessionRepository;
import com.esmem.esmem.tools.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class SessionController{
    @Autowired
    SessionRepository repository;
    @Autowired
    UserRepository userRepository;

    @RequestMapping(method = RequestMethod.POST,path = "/login")
    public Long login(@RequestBody LoginAttempt loginAttempt){
        Optional<User> user = userRepository.findById(loginAttempt.getUsername());
        if(user.isPresent() && user.get().getPassword().equals(loginAttempt.getPassword())){
            Session session = new Session(user.get().getUsername());
            repository.save(session);
            return session.getSessionId();
        }
        else{
            return new Long(0);
        }
    }


    @RequestMapping(method = RequestMethod.GET, path = "/logout")
    public void logout( @RequestHeader("Session-Id") Long sessionId){
        repository.deleteById(sessionId);
    }




}

class LoginAttempt{
    private String password;
    private String username;

    public LoginAttempt(String password, String username) {
        this.password = password;
        this.username = username;
    }

    public LoginAttempt() {
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}


