package com.esmem.esmem.tools.Controller;

import com.esmem.esmem.tools.Model.Session;
import com.esmem.esmem.tools.Model.Tool;
import com.esmem.esmem.tools.Repository.SessionRepository;
import com.esmem.esmem.tools.Repository.ToolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class ToolController {

    @Autowired
    ToolRepository repository; //gives accesss to the repository
    @Autowired
    SessionRepository sessionRepository;

    @CrossOrigin
    @RequestMapping(path="/tool", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody Iterable<Tool> getTools(){
        return repository.findAll();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.POST, path = "/tool")
    public Tool addTool(@RequestBody Tool tool,  @RequestHeader("Session-Id") Long sessionId ){
        Optional<Session> session = sessionRepository.findById(sessionId);
        if(session.isPresent()){
            tool.setOwner(session.get().getUserId());
            repository.save(tool);
        }
        return tool;
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/tool/{id}/borrow")
    public Tool borrowTool(@PathVariable("id") Long id, @RequestHeader("Session-Id") Long sessionId ){
        Optional<Tool> oldTool = repository.findById(id);
        Optional<Session> session = sessionRepository.findById(sessionId);
        if(oldTool.isPresent() && session.isPresent())
        {
            oldTool.get().setBorrowedById(session.get().getUserId());
            repository.save(oldTool.get());
        }
        return new Tool();
    }

    @CrossOrigin
    @RequestMapping(method = RequestMethod.GET, path = "/tool/{id}/return")
    public Tool returnTool(@PathVariable("id") Long id, @RequestHeader("Session-Id") Long sessionId){
        Optional<Tool> oldTool = repository.findById(id);
        Optional<Session> session = sessionRepository.findById(sessionId);
        if(oldTool.isPresent() && session.isPresent() && oldTool.get().getBorrowedById().equals(session.get().getUserId()))
        {
            oldTool.get().setBorrowedById(null);
            repository.save(oldTool.get());
        }
        return new Tool();
    }



}
